import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

    likeValue: number;
    dislikeValue: number;
    medium: number = 0;
    showText: boolean = false;
    moreOrLess: string = "+";

  constructor(){
  this.likeValue = 0;
  this.dislikeValue = 0;
  }

  handleLike(){
   this.likeValue++;
   this.calculateMedium();
  }
  handleDislike(){
   this.dislikeValue++;
   this.calculateMedium();
  }
  calculateMedium(){
  this.medium = Math.round(this.likeValue/(this.likeValue+this.dislikeValue)*10*100)/100;
  }
  handleMoreOrLess(){
    this.showText = !this.showText;
    if(this.moreOrLess === "+"){
      this.moreOrLess = "-";
    }else{
      this.moreOrLess = "+";
    }

  }
}

