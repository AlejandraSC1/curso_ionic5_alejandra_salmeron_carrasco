const users = require("./users.json");

//
// TODO 1
// Obtener el usuario cuyo teléfono es "024-648-3804"
//
console.log("=== 1 ===");

const phoneWanted = users.find((user) => user.phone === "024-648-3804");

console.log(phoneWanted);

//
// TODO 2
// Crear una función que devuelva true si existe un usuario cuyo email
// sea el que se pasa como parámetro
//
console.log("=== 2 ===");

const existsUser = (email) => {
  return users.some((user) => user.email === email);
};

console.log(existsUser("Nathan@yesenia.net")); // true
console.log(existsUser("japostigo@atsistemas.com")); // false

//
// TODO 3
// Obtener el número de usuarios que tienen website
//
console.log("=== 3 ===");

const usersWithWebsite = users.filter(
  (user) => user.website && user.website != undefined
);
console.log(usersWithWebsite.length);

//
// TODO 4
// Obtener el índice de la posición que toma en el array el primer usuario
// cuyo número de la calle de su dirección es menor que 300
//
console.log("=== 4 ===");

const obtainHomeNumber = users.findIndex((user) => user.address.number < 300);
console.log(obtainHomeNumber);

//
// TODO 5
// Obtener un array que sólo contenga las cadenas de los emails de los usuarios
//
console.log("=== 5 ===");

const usersEmails = users.map((user) => user.email);
console.log(usersEmails);

//
// TODO 6
// Obtener un array que contengan objetos {id: "id", username: "username"},
// que contienen los ids y los nombres de usuarios de los usuarios
//
console.log("=== 6 ===");

const obtainObject = (user) => {
  let object = { id: user.id, username: user.username };
  return object;
};

const idNameUsers = users.map((user) => obtainObject(user));

console.log(idNameUsers);

//
// TODO 7
// Obtener el array de usuarios pero con los números de sus direcciones en
// formato de número (y no de cadena que es como está ahora mismo)
//
console.log("=== 7 ===");

const changeToNumber = (user) => {
  if (user.address) {
    user.address.number = parseInt(user.address.number);
  }
  return user;
};

const addressNumber = users.map((user) => changeToNumber(user));
console.log(addressNumber);

//
// TODO 8
// Obtener el array de usuarios cuya dirección está ubicada entre la
// latitud -50 y 50, y la longitud -100 y 100
//
console.log("=== 8 ===");

const addressUser = users.filter((user) => user.address);
const addressGeo = addressUser.filter(
  (user) =>
    user.address.geo.lat > -50 &&
    user.address.geo.lat < 50 &&
    user.address.geo.lng > -100 &&
    user.address.geo.lng < 100
);
console.log(addressGeo);

//
// TODO 9
// Obtener un array con los teléfonos de los usuarios cuyo website
// pertenezca a un dominio biz
//
console.log("=== 9 ===");
const biz = ".biz";
const filterBizWebsite = users.filter(
  (user) => user.website && user.website.includes(biz)
);
const phoneWithBizWebsite = filterBizWebsite.map(
  (filterBizWebsite) => filterBizWebsite.phone
);
console.log(phoneWithBizWebsite);

//
// TODO 10
// Escriba una función processArray que, dado un array de números
// enteros, devuelva un nuevo array en que aquellos elementos que
// sean pares se multipliquen por 2.
//
console.log("=== 10 ===");

const testArray = [2, 3, 5, 6, 5, 9, 10, 12, 13];

const processArray = (testArray) => {
  const pairs = testArray.filter((number) => number % 2 === 0);
  const pairsMultiply = pairs.map((pair) => pair * 2);

  return pairsMultiply;
};

console.log(processArray(testArray)); // [4, 12, 20, 24]

