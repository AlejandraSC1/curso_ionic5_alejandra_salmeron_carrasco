//
// TODO 1
// Declarar en ES6 la clase Persona
//
console.log("=== 1 ===");

const Person = (name, age) => ({ name: name, age: age });

//
Person.prototype.greet = () => {
  console.log(`Hi, my name is ${name} and I\'m ${age} years old.`);
};

//
Person.prototype.haveABirthday = () => {
  this.age++;
};

//
let p = new Person("Posti", 36);

p.haveABirthday();
p.greet();

//
// TODO 2
// Crear un modulo que contenga una clase coche con dos atributos: make y km.
// El constructor sólo aceptaría como parámetro make, porque km se inicia a 0.
// También dispondría de dos métoodos: move(km) que incrementa los km según
// la cantidad y getInfo(), que devuelve un string con el make y el número de km
//
console.log("=== 2 ===");


//
// TODO 3
// Use funciones flecha en vez de las que se indican
//
console.log("=== 3 ===");

const array = [1, 2, 3];

array.map = (x) => {
  return x * 2;
};

console.log(array);


//
// TODO 4
// Declare las variables convenientemente
//

console.log("=== 4 ===");

let PI = 3.14;
let PI = 3.14;

function getCircleArea(r) {
  return PI * r * r;
}

for (let i = 1; i <= 3; i++) {
  console.log("r = " + i + ", area = " + getCircleArea(i));
}

Person.prototype.haveABirthday = () => {
  this.age++;
};

//
// TODO 5
// Simplifique este objeto usando sintaxis ES6
//

console.log("=== 5 ===");

let prop2 = "value2";

const object1 = {
  prop1: "value1",

  prop2: prop2,

  function1: function (param) {
    if (typeof param == "undefined") {
      param = 1;
    }

    console.log(param);
  },
};

console.log(object1.prop1);
console.log(object1.prop2);
object1.function1();

//
// TODO 6
// Simplifique estas asignaciones a variables usando sintaxis ES6
//

console.log("=== 6 ===");

let object2 = {
  a: 1,
  b: 2,
  c: 3,
  d: 4,
  e: 5,
  f: 6,
};

let a = object2.a;
let b = object2.b;
let c = object2.c;
let d = object2.d;
let e = object2.e;
let f = object2.f;

console.log(a, b, c, d, e, f);

//
// TODO 7
// Simplifique las siguientes asignaciones usando el spread operator
//

console.log("=== 7 ===");

const a1 = [4, 5, 6, 7, 8, 9, 10];

const a2 = [0, 1, 2, 3];

a1.forEach(function (x) {
  a2.push(x);
});

console.log(a2);

//
// TODO 8
// Convierta los siguientes bucles usando for of y for in
//

console.log("=== 8 ===");

const a3 = [0, 1, 2, 3, 4, 5];

for (i = 0; i < a3.length; i++) {
  console.log(a3[i]);
}

const object3 = {
  a: 1,
  b: 2,
  c: 3,
};

const objKeys = Object.keys(object3);

for (i = 0; i < objKeys.length; i++) {
  if (object3.hasOwnProperty(objKeys[i])) {
    console.log(objKeys[i] + ": " + object3[objKeys[i]]);
  }
}

