import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Expandable', url: '/components/expandable'},
    { title: 'Ratings', url: '/components/ratings'},
    { title: 'Fruits', url: '/components/fruits' }
  ]
  constructor() {}
}
