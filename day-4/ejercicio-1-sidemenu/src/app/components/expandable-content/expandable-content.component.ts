import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-expandable-content',
  templateUrl: './expandable-content.component.html',
  styleUrls: ['./expandable-content.component.scss'],
})
export class ExpandableContentComponent implements OnInit {
  showText: boolean = false
  moreOrLess: string = "+";
  @Input() title: string;
  @Input() content: string;
  @Output() newItemEvent = new EventEmitter<number>();
  constructor() { }

  ngOnInit() {}
  handleMoreOrLess(){
    this.showText = !this.showText;
    if(this.moreOrLess === "+"){
      this.moreOrLess = "-";
      this.newItemEvent.emit(1);
    }else{
      this.moreOrLess = "+";
      this.newItemEvent.emit(-1);
    }
  }
}
