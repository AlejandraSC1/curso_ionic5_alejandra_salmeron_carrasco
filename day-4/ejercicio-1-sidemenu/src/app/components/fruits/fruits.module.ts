import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FruitsPageRoutingModule } from './fruits-routing.module';

import { FruitsPage } from './fruits.page';
import { ExpandableContentComponent } from '../expandable-content/expandable-content.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FruitsPageRoutingModule
  ],
  declarations: [FruitsPage, ExpandableContentComponent]
})
export class FruitsPageModule {}
