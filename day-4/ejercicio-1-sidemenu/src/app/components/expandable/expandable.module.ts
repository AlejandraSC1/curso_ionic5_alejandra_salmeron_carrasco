import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExpandablePageRoutingModule } from './expandable-routing.module';

import { ExpandablePage } from './expandable.page';
import { ExpandableContentComponent } from '../expandable-content/expandable-content.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExpandablePageRoutingModule
  ],
  declarations: [ExpandablePage, ExpandableContentComponent]
})
export class ExpandablePageModule {}
