import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-expandable',
  templateUrl: './expandable.page.html',
  styleUrls: ['./expandable.page.scss'],
})
export class ExpandablePage implements OnInit {
  showText: boolean = false;
  numberOpenedAccordeons: number = 0;
  moreOrLess: string = "+";
constructor() { }

  ngOnInit() {
  }

  handleMoreOrLess(){
    this.showText = !this.showText;
    if(this.moreOrLess === "+"){
      this.moreOrLess = "-";
    }else{
      this.moreOrLess = "+";
    }
  }
  addItem(number: number){
    this.numberOpenedAccordeons += number;
  }
}
