import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-rating-component',
  templateUrl: './rating-component.component.html',
  styleUrls: ['./rating-component.component.scss'],
})
export class RatingComponent implements OnInit {
  @Input() likeValue: number;
  @Input() dislikeValue: number;
  showText: boolean = false;
  medium: number = 0;
  constructor() {}
  handleLike() {
    this.likeValue++;
    this.calculateMedium();
  }
  handleDislike() {
    this.dislikeValue++;
    this.calculateMedium();
  }
  calculateMedium() {
    let totalValues =
      Math.round(this.dislikeValue) + Math.round(this.likeValue);
    if (totalValues != 0 && totalValues != undefined) {
      this.medium = Math.round((this.likeValue / totalValues) * 1000) / 100;
    } else {
      this.medium = 0;
    }
  }
  ngOnInit() {

    if (!this.likeValue && !this.dislikeValue) {
      this.likeValue = 0;
      this.dislikeValue = 0;
      this.calculateMedium();
    } else if (!this.likeValue) {
      this.likeValue = 0;
      this.calculateMedium();
    } else if (!this.dislikeValue) {
      this.dislikeValue = 0;
      this.calculateMedium();
    } else {
      this.calculateMedium();
    }
  }
}
