import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'components/expandable',
    pathMatch: 'full'
  },
   {
    path: 'components/expandable',
    loadChildren: () => import('./components/expandable/expandable.module').then( m => m.ExpandablePageModule)
  },
  {
    path: 'components/ratings',
    loadChildren: () => import('./components/ratings/ratings.module').then( m => m.RatingsPageModule)
  },
  {
    path: 'components/fruits',
    loadChildren: () => import('./components/fruits/fruits.module').then( m => m.FruitsPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
